/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RecruitmentSystem.Util;

/**
 *
 * @author UserPc
 */
public class UserPost {
    
    private String firstName;   
    private String lastName;
    private String email; 
    private String link;   
    private String name;
    private String requirements;
    private String status;

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmail() {
        return email;
    }

    public String getLink() {
        return link;
    }

    public String getName() {
        return name;
    }

    public String getRequirements() {
        return requirements;
    }

    public String getStatus() {
        return status;
    }
    

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setRequirements(String requirements) {
        this.requirements = requirements;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    
}
