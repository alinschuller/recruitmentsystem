/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RecruitmentSystem.Util;

/**
 *
 * @author UserPc
 */
public class UserDetails implements java.io.Serializable{
   
    private Long id;
    private String email;   
    private String password;    
    private String link;   
    private String firstName;   
    private String lastName;
    private Integer roleId;
    private Integer statusId;
    
    public UserDetails(Long id, String email, String password, String link, 
                        String firstName, String lastName, Integer roleId, Integer statusId) {
        this.id = id;
        this.email = email;
        this.password = password;
        this.link = link;
        this.firstName = firstName;
        this.lastName = lastName;
        this.roleId = roleId;
        this.statusId = statusId;
    }

    public Long getId() {
        return id;
    }
    
    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public String getLink() {
        return link;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public Integer getRoleId(){
        return roleId;
    }
    
    public Integer getStatusId() {
        return statusId;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }
    
    public void setStatusId(Integer statusId) {
        this.statusId = statusId;
    }
    
    @Override
    public String toString() {
        return "UserDetail{ id=" + id + ", email=" + email + ", password=" + 
                password + ", link=" + link + ", firstName=" + firstName + 
                ", lastName=" + lastName + '}';
    } 
}
