/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RecruitmentSystem.Util;

/**
 *
 * @author UserPc
 */
public class PostDetails implements java.io.Serializable{
    private Long id;
    private String name;
    private String requirements;

    public PostDetails(Long id, String name, String requirements) {
        this.id = id;
        this.name = name;
        this.requirements = requirements;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getRequirements() {
        return requirements;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setRequirements(String requirements) {
        this.requirements = requirements;
    }
    
}
