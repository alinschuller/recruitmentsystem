/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RecruitmentSystem.Servlet;

import RecruitmentSystem.Entity.RSRole;
import RecruitmentSystem.Request.Request;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import RecruitmentSystem.Request.RequestBean;
import RecruitmentSystem.Util.PostDetails;
import RecruitmentSystem.Util.UserDetails;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJBException;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.inject.Inject;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Samuel
 */
@WebServlet(name = "LoginServlet", urlPatterns = "LoginServlet") 
public class LoginServlet extends HttpServlet {
    @Inject
    Request requestBean;
    
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        HttpSession session = request.getSession(false);
        
        String email = request.getParameter("email");
        String pass = request.getParameter("password");
        
        
        UserDetails user;
        user = requestBean.getUser(email);
        if(user != null){
            if(pass.equals(user.getPassword())){
                
                session.setAttribute("userEmail", user.getEmail());
                session.setAttribute("userRoleId", user.getRoleId());
                if(RSRole.getType(user.getRoleId()).toString().equals("GENERAL_DIRECTOR")){
                    response.sendRedirect("ViewList");
                } else {
                    String link = user.getLink();
                    if(link == null){
                        response.sendRedirect("uploadcv.jsp");
                    }else{
                        session.setAttribute("CV", user.getLink());
                        String post;
                        post= requestBean.getPostName(user);
                        if(post != null) {
                            response.sendRedirect("CandidatePost?postNameIndex=" + post);
                        } else {
                            response.sendRedirect("ApplyToPost");
                        }
                    }
                }
            }else{
                response.sendRedirect("index.jsp?error=Password are wrong!");
            }
        }else{
            response.sendRedirect("index.jsp?error=Email is wrong!");
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private RequestBean lookupRequestBeanBean() {
        try {
            Context c = new InitialContext();
            return (RequestBean) c.lookup("java:global/com.csrs_RecruitmentSystem_war_1.0-SNAPSHOT/RequestBean!RecruitmentSystem.Request.RequestBean");
        } catch (NamingException ne) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "exception caught", ne);
            throw new RuntimeException(ne);
        }
    }

}
