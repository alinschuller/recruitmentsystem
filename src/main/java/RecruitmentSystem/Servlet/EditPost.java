/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RecruitmentSystem.Servlet;

import RecruitmentSystem.Entity.RSRole;
import RecruitmentSystem.Request.Request;
import RecruitmentSystem.Util.PostDetails;
import RecruitmentSystem.Util.UserDetails;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Samuel
 */
@WebServlet(name = "EditPost", urlPatterns = {"/EditPost"})
public class EditPost extends HttpServlet {

    @Inject
    private Request repository;
    
    private static final Logger LOG = Logger.getLogger(UploadCv.class.getName());
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet EditPost</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet EditPost at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        if(request.isRequestedSessionIdValid()) {
            HttpSession session = request.getSession();
            String type = "";
            if(session.getAttribute("userEmail") != null)
                type = RSRole.getType(((UserDetails)repository.getUser((String)session.getAttribute("userEmail"))).getRoleId()).toString();
            if(type.equals("GENERAL_DIRECTOR")) {
                List<PostDetails> allPosts = repository.getAllPosts();
                request.setAttribute("allPosts", allPosts);
                request.getRequestDispatcher("editPost.jsp").forward(request, response);
            } else {
                response.sendRedirect("index.jsp");
            }
        } else {
            response.sendRedirect("index.jsp");
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        if(request.isRequestedSessionIdValid()) {
            HttpSession session = request.getSession();
            String type = "";
            if(session.getAttribute("userEmail") != null)
                type = RSRole.getType(((UserDetails)repository.getUser((String)session.getAttribute("userEmail"))).getRoleId()).toString();
            if(type.equals("GENERAL_DIRECTOR")) {
                String name = request.getParameter("name");
                name = name.substring(1, name.length());
                String req = request.getParameter("req");
                LOG.info(name + " " + req);
                PostDetails post = repository.getPost(name);
                String reqOld = post.getRequirements();
                if(req != reqOld){
                    post.setRequirements(req);
                    boolean update = repository.updatePost(post);
                    String  msg;
                    if(update){
                        msg = "Succes updated post " + post.getName();
                        LOG.info(msg);
                    } else {
                        msg = "Error updating post " + post.getName();
                        LOG.info(msg);
                    }
                    response.sendRedirect("EditPost");
                } else {
                    response.sendRedirect("EditPost");
                }
            } else {
                response.sendRedirect("index.jsp");
            }
        } else {
            response.sendRedirect("index.jsp");
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
