/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RecruitmentSystem.Servlet;

import RecruitmentSystem.Entity.RSStatus;
import RecruitmentSystem.Request.Request;
import RecruitmentSystem.Util.PostDetails;
import RecruitmentSystem.Util.UserDetails;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJBException;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author UserPc
 */
@WebServlet(name = "CandidatePost", urlPatterns = {"/CandidatePost"})
public class CandidatePost extends HttpServlet {
    
    @Inject
    private Request repository;
    
    private static final Logger LOG = Logger.getLogger(CandidatePost.class.getName());
    

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet CandidatePost</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet CandidatePost at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        if(request.isRequestedSessionIdValid()){
            String postName = (String) request.getParameter("postName");
            String oldPost = (String) request.getParameter("postNameIndex");
            LOG.log(Level.INFO, "Apply to post! Candidate post name: {0}", postName);
            
            HttpSession session = request.getSession(false);
            String email = (String) session.getAttribute("userEmail");
            String linkToCV = (String) session.getAttribute("CV");
            LOG.log(Level.INFO, "email: {0}", email);
            UserDetails user = repository.getUser(email);
            LOG.log(Level.INFO, "linkToCV: {0}", linkToCV);
            
            PostDetails post;
            String msg = "";
            if(linkToCV != null){
                if(oldPost != null){
                    post = repository.getPost(oldPost);
                } else if(postName == null){
                    post = repository.getPost((String) repository.getPostName(user));
                   
                    LOG.log(Level.INFO, "post: name: {0}", post.getName());

                    msg = "You have applied to post: ";
                } else {
                    post = repository.getPost(postName);
                    oldPost = repository.getPostName(user);

                    if(oldPost != null){
                        LOG.info("We are here to discard a post!");
                        repository.discardFromPost(user.getId(), repository.getPost(oldPost).getId());
                    }
                    boolean apply = repository.applyToPost(user.getId(), post.getId());                         
                    if(!apply){
                        msg = "Sorry... Something happened in aplying to post!";
                    } else {
                        msg = "Success applied to post: " + postName + " !";
                        session.setAttribute("post", repository.getPostName(user));
                        Integer statusId = repository.getStatusByName("wait");
                        LOG.log(Level.INFO, "statusId: {0}", statusId);
                        user.setStatusId(statusId);
                        repository.updateUser(user);
                    }               
                }
                LOG.log(Level.INFO, "Verify post: {0}", post.getName());
                request.setAttribute("postName", post.getName());
                request.setAttribute("postReq", post.getRequirements());
                request.setAttribute("postStatus", RSStatus.getType(user.getStatusId()).toString());
            } else {
                msg = "Please upload your CV first!";
            }
            request.setAttribute("message", msg);           
            request.getRequestDispatcher("candidatePost.jsp").forward(request, response);
        } else {
            response.sendRedirect("index.jsp");
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
