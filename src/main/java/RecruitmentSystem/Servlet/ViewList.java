/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RecruitmentSystem.Servlet;

import RecruitmentSystem.Entity.RSStatus;
import RecruitmentSystem.Request.Request;
import RecruitmentSystem.Util.PostDetails;
import RecruitmentSystem.Util.UserDetails;
import RecruitmentSystem.Util.UserPost;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author UserPc
 */
@WebServlet(name = "ViewList", urlPatterns = {"/ViewList"})
public class ViewList extends HttpServlet {
    
    @Inject
    private Request repository;
    
    private static final Logger LOG = Logger.getLogger(ViewList.class.getName());

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ViewList</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ViewList at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        if(request.isRequestedSessionIdValid()){
            //when a user has been accepted or dennied...
            String processed = (String) request.getParameter("processed");
            String userEmail = (String) request.getParameter("email");
            String status = (String) request.getParameter("status");
            
            LOG.log(Level.INFO, "processed: {0}", processed);
            LOG.log(Level.INFO, "userEmail: {0}", userEmail);
            LOG.log(Level.INFO, "status: {0}", status);
            
            if("true".equals(processed)){
                UserDetails user = repository.getUser(userEmail);
                String msg;
                String oldStatus = repository.getStatus(user.getStatusId());
                if(!status.equals(oldStatus)){
                    user.setStatusId(RSStatus.getType(status).getId());
                    msg = user.getFirstName() + "'s status updated: " + status;
                } else {
                    msg = user.getFirstName() + " has to upload the CV or to apply to a post! ";
                }
                request.setAttribute("msg", msg);
                repository.updateUser(user);
            }
            //
            
            //table with all candidates
            List<UserPost> allUsersPosts = repository.getAllUsersPosts("all", "all");
            request.setAttribute("postMsg", "to all posts");
            request.setAttribute("allUsersPosts", allUsersPosts);
            //
            
            //to display the post options
            List<PostDetails> allPosts = repository.getAllPosts();
            request.setAttribute("allPosts", allPosts);
            //
            
            //to display the status options
            List<String> allStatuses = repository.getAllStatuses();
            request.setAttribute("allStatuses", allStatuses);
            //
            
            request.getRequestDispatcher("viewList.jsp").forward(request, response);
        } else {
            response.sendRedirect("index.jsp");
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        //table with the candidates applied to this post with this status
        String thisPostName = request.getParameter("selectedPost");
        String thisStatusName = request.getParameter("selectedStatus");
        List<UserPost> allUsersPosts = repository.getAllUsersPosts(thisPostName, thisStatusName);
        request.setAttribute("allUsersPosts", allUsersPosts);
        
        String postMsg;
        if("all".equals(thisPostName)){
            postMsg = " to all posts ";
        } else {
            postMsg = "to " + thisPostName + " post ";
        }
        request.setAttribute("postMsg", postMsg);
        
        String statusMsg;
        if("all".equals(thisStatusName)){
            statusMsg = " with all statuses";
        } else {
            statusMsg = " with " + thisStatusName + " status";
        }
        request.setAttribute("statusMsg", statusMsg);
        //
        
        //to display the post options
        List<PostDetails> allPosts = repository.getAllPosts();
        request.setAttribute("allPosts", allPosts);
        //
        
        //to display the status options
        List<String> allStatuses = repository.getAllStatuses();
        request.setAttribute("allStatuses", allStatuses);
        //
        
        request.getRequestDispatcher("viewList.jsp").forward(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
