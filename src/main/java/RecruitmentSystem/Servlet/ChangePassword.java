/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RecruitmentSystem.Servlet;

import RecruitmentSystem.Request.Request;
import RecruitmentSystem.Util.UserDetails;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Logger;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author UserPc
 */
@WebServlet(name = "ChangePassword", urlPatterns = {"/ChangePassword"})
public class ChangePassword extends HttpServlet {

    @Inject
    private Request repository;
    
    private static final Logger LOG = Logger.getLogger(ChangePassword.class.getName());
    
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ChangePassword</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ChangePassword at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        if(request.isRequestedSessionIdValid()){
            HttpSession session = request.getSession(false);
            String email = (String) session.getAttribute("userEmail");
            UserDetails user = repository.getUser(email);
            String role = repository.getRole(user.getRoleId());
            if("candidate".equalsIgnoreCase(role)){
                request.setAttribute("menu", "headerCandidate");
            } else {
                request.setAttribute("menu", "headerDirector");
            }
            
            request.getRequestDispatcher("changePassword.jsp").forward(request, response);
        } else {
            response.sendRedirect("index.jsp");
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String oldPassword = request.getParameter("oldPassword");
        String newPassword = request.getParameter("newPassword");
        String confirmPassword = request.getParameter("confirmPassword");
        
        HttpSession session = request.getSession(false);
        String email = (String) session.getAttribute("userEmail");
        
        UserDetails user = repository.getUser(email);
        String msg;
        if(newPassword.equals(confirmPassword)){
            if(oldPassword.equals(user.getPassword())){
                if(newPassword.length() < 5){
                    msg = "The new password is too short! It must have minumm 5 characters!";
                } else {
                    user.setPassword(newPassword);
                    if(repository.updateUser(user)){
                        msg = "Password successful changed!";
                    } else {
                        msg = "Error in update new password!";
                    }
                }
            } else {
                msg = "The old password doesn't match!";
            }
        } else {
            msg = "The new password is not the same!";
        }
        String link = "ChangePassword?changePassMsg=" + msg;
        response.sendRedirect(link);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
