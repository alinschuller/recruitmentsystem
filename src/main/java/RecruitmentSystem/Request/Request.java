/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RecruitmentSystem.Request;

import RecruitmentSystem.Util.PostDetails;
import RecruitmentSystem.Util.UserDetails;
import RecruitmentSystem.Util.UserPost;
import java.util.List;

/**
 *
 * @author UserPc
 */

public interface Request {
    
    boolean createUser(String email, String password, String link, String firstName, 
                String lastName, String roleName, String status);
    
    UserDetails getUser(String email);
    
    UserDetails getUser(String firstName, String lastName);
    
    List<UserDetails> findAllUsers();
    
    List<UserDetails> findAllUsersByRole(String roleName);
    
    List<UserDetails> findAllUsersByStatus(String status);
    
    /**
     *
     * @param newUser
     * @return
     * True if the entity was updated, false otherwise
     */
    boolean updateUser(UserDetails newUser);
    
    /**
     * @param id
     * @return
     * Delete the specified RSUser entity. Returns true if the operation was
     * successful committed, false otherwise.
     */
    boolean deleteUser(Long id);
    
    boolean createPost(String name, String requirements);
    
    PostDetails getPost(String name);
    
    String getPostName(UserDetails user);
    
    boolean updatePost(PostDetails newPost);
    
    boolean deletePost(Long id);
    
    boolean applyToPost(Long userId, Long postId);
    
    boolean discardFromPost(Long userId, Long postId);
        
    List<UserDetails> getUsersApplingToPost(String postName, String statusName);
    
    String getRole(Integer id);
    
    String getStatus(Integer id);
    
    Integer getStatusByName(String status);
    
    List<String> getAllStatuses();
    
    List<PostDetails> getAllPosts();
    
    List<UserPost> getAllUsersPosts(String postName, String statusName);
    
}
