/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RecruitmentSystem.Request;

import RecruitmentSystem.Entity.RSPost;
import RecruitmentSystem.Entity.RSRole;
import RecruitmentSystem.Entity.RSStatus;
import RecruitmentSystem.Entity.RSUser;
import RecruitmentSystem.Util.UserDetails;
import RecruitmentSystem.Util.PostDetails;
import RecruitmentSystem.Util.UserPost;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author UserPc
 */
@Stateful
public class RequestBean implements Request{
    private static final Logger LOG = Logger.getLogger(RequestBean.class.getName());
    
    @PersistenceContext
    private EntityManager em;
    
    @Override
    public boolean createUser(String email, String password, String link, 
                           String firstName, String lastName, String roleName, String status) {
        LOG.info("createUser");      
        try{
            int statusId = RSStatus.getType(status).getId();
            int roleId = RSRole.getType(roleName).getId();
            RSUser user = new RSUser(email, password, link, firstName, lastName,
                                    roleId, statusId);
            em.persist(user);
            return true;
        } catch (Exception e){
            LOG.info("ERROR in createUser");
            LOG.info(e.getMessage());
        }
        return false;
    }
    
    @Override
    public boolean createPost(String name, String requirements) {
        LOG.info("createPost");        
        try{
            RSPost post = new RSPost(name, requirements);
            em.persist(post);
            return true;
        } catch (Exception e){
            LOG.info("ERROR in createPost");
        }
        return false;
    }

    @Override
    public List<UserDetails> findAllUsers() {
        LOG.info("findAllUsers");
        List<UserDetails> users = new ArrayList<>();
        try{
            Query query = em.createQuery("SELECT u FROM RSUser u");
            users = this.copyRSUserToUserDetails((List<RSUser>) query.getResultList());
        } catch (Exception e){
            LOG.info("ERROR in findAllUsers");
        } 
        return users;
    }

    private List<UserDetails> copyRSUserToUserDetails(List<RSUser> users){
        List<UserDetails> userDetails = new ArrayList<>();
        for (RSUser user : users) {
            userDetails.add(copyRSUserToUserDetails(user));
        }
        return userDetails;
    }
    
    @Override
    public List<UserDetails> findAllUsersByRole(String roleName) {
        LOG.info("findAllUsersByRole");
        List<UserDetails> users = new ArrayList<>();
        try{
            Integer roleId = RSRole.valueOf(roleName).getId();
            Query query = em.createQuery("SELECT u FROM RSUser u WHERE "
                                        + "u.roleId = :id");
            query.setParameter("id", roleId);
            users = this.copyRSUserToUserDetails((List<RSUser>)query.getResultList());
        } catch (Exception e){
            LOG.info("ERROR in findAllUsersByRole");
        } 
        return users;
    }
    
    @Override
    public List<UserDetails> findAllUsersByStatus(String status) {
        LOG.info("findAllUsersByStatus");
        List<UserDetails> users = new ArrayList<>();
        try {
            Integer statusId = RSStatus.getType(status).getId();
            Query query = em.createQuery("SELECT u FROM RSUser u WHERE u.statusId = :id");
            query.setParameter("id", statusId);
            users = this.copyRSUserToUserDetails((List<RSUser>)query.getResultList());
        } catch (Exception e) {
            LOG.info("ERROR in findAllUsersByStatus");
        }
        return users;
    }

    @Override
    public UserDetails getUser(String email) {
        LOG.info("getUser by email");
        UserDetails user = null;
        try{
            Query query = em.createQuery("SELECT u FROM RSUser u WHERE "
                                        + "u.email = :userEmail");
            query.setParameter("userEmail", email);
            RSUser rsuser = (RSUser) query.getSingleResult();
            LOG.log(Level.INFO, "rsuser first name: {0}", rsuser.getFirstName());
            user = this.copyRSUserToUserDetails(rsuser);
        } catch (Exception e){
            LOG.log(Level.INFO, "ERROR in getUser by email: {0}", email);
            System.out.println(e);
        } 
        return user;
    }
    
    private UserDetails copyRSUserToUserDetails(RSUser user){
        UserDetails userDetails = new UserDetails(user.getId(), user.getEmail(),
                                        user.getPassword(), user.getLink(),
                                        user.getFirstName(), user.getLastName(),
                                        user.getRole().getId(), user.getStatus().getId());
        return userDetails;
    }

    @Override
    public UserDetails getUser(String firstName, String lastName) {
        LOG.info("getUser by firstName and lastName");
        UserDetails user = null;
        try{
            Query query = em.createQuery("SELECT u FROM RSUser u WHERE "
                    + "u.firstName = :firstName AND u.lastName = :lastName");
            query.setParameter("firstName", firstName);
            query.setParameter("lastName", lastName);
            user = this.copyRSUserToUserDetails((RSUser) query.getSingleResult());
        } catch (Exception e){
            LOG.log(Level.SEVERE, "ERROR in getUser by firstName and lastName: {0}", e);
        } 
        return user;
    }

    @Override
    public boolean updateUser(UserDetails newUser) {
        LOG.info("updateUser");
        try{           
            RSStatus status = RSStatus.getType(newUser.getStatusId());
            RSUser updateUser = (RSUser) em.find(RSUser.class, newUser.getId());
            updateUser.setEmail(newUser.getEmail());
            updateUser.setFirstName(newUser.getFirstName());
            updateUser.setLastName(newUser.getLastName());
            updateUser.setLink(newUser.getLink());
            updateUser.setPassword(newUser.getPassword()); 
 
            updateUser.setStatus(status);
            return true;
        } catch (Exception e){
            LOG.log(Level.INFO, "ERROR in updateUser: {0}", e);
            em.getTransaction().rollback();
        } 
        return false;
    }

    @Override
    public boolean deleteUser(Long id) {
        LOG.info("deleteUser");
        try{    
//            RSUser userToBeDeleted;
//            userToBeDeleted = em.getReference(RSUser.class, user.getId());
            RSUser user = em.find(RSUser.class, id);
            em.remove(user);
            return true;
        } catch (Exception e){
            LOG.info("ERROR in updateUser");
        } 
        return false;
    }

    @Override
    public PostDetails getPost(String name) {
        LOG.info("getPost by name");
        PostDetails post;
        post = null;
        try{
            Query query = em.createQuery("SELECT p FROM RSPost p WHERE p.name =:name");
            query.setParameter("name", name);
            post = this.copyRSPostToPostDetails((RSPost) query.getSingleResult());
        } catch (Exception e){
            LOG.info("ERROR in getPost by name");
        } 
        return post;
    }
    
    @Override
    public String getPostName(UserDetails user){
        RSUser rsUser = em.find(RSUser.class, user.getId());
        if(rsUser.getPost() != null)
            return rsUser.getPost().getName();
        return null;
    }
    
    private PostDetails copyRSPostToPostDetails(RSPost post){
        PostDetails postDetails = new PostDetails(post.getId(), post.getName(), 
                                                    post.getRequirements());
        return postDetails;
    }

    @Override
    public boolean updatePost(PostDetails newPost) {
        LOG.info("updatePost");
        try{
            Query query = em.createQuery("UPDATE RSPost p SET p.name = :name, "
                    + "p.requirements = :requirements WHERE p.id = :id");
            query.setParameter("name", newPost.getName());
            query.setParameter("requirements", newPost.getRequirements());
            query.setParameter("id", newPost.getId());
            Integer returnedId = query.executeUpdate();
            if(returnedId > 0){
                return true;
            }
        } catch (Exception e){
            em.getTransaction().rollback();
            LOG.info("ERROR in updateUser");
        } 
        return false;
    }

    @Override
    public boolean deletePost(Long id) {
        LOG.info("deletePost");
        try{

//            RSPost postToBeDeleted;
//            postToBeDeleted = em.getReference(RSPost.class, post.getId());
            RSPost post = em.find(RSPost.class, id);
            Collection<RSUser> users = post.getUsers();
            for(RSUser user : users){
                post.dropUser(user);
            }
            
            em.remove(post);
            return true;
        } catch (Exception e){
            LOG.info("ERROR in updateUser");
        } 
        return false;
    }

    @Override
    public List<UserDetails> getUsersApplingToPost(String postName, String statusName) {
        LOG.log(Level.INFO, "getUsersApplingToPost {0}", postName);
        List<UserDetails> finalUsers = new ArrayList<>();
        try{
            PostDetails postDetails = getPost(postName);
            RSPost post = em.find(RSPost.class, postDetails.getId());
            List<UserDetails> usersDetails = this.copyRSUserCollectionToUserDetails(post.getUsers());
            
            if(!"all".equals(statusName)){
                for(UserDetails user : usersDetails){
                    String userStatus = RSStatus.getType(user.getStatusId()).name();
                    if(userStatus.equals(statusName)){
                        finalUsers.add(user);
                    }
                }
            } else {
                finalUsers = usersDetails;
            }
        } catch(Exception e){
            LOG.log(Level.SEVERE, "ERROR in getUsersApplingToPost: {0}", e);
        }
        return finalUsers;
    }
    
    private List<UserDetails> copyRSUserCollectionToUserDetails(Collection<RSUser> users){
        List<UserDetails> userDetails = new ArrayList<>();
        Iterator i = users.iterator();
        while(i.hasNext()){
            RSUser user = (RSUser) i.next();
            userDetails.add(copyRSUserToUserDetails(user));
        }
        return userDetails;
    }
    
    private boolean apply(Collection<RSUser> users, Long userId){
        for(RSUser user : users){
            if(Objects.equals(user.getId(), userId)){
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean applyToPost(Long userId, Long postId) {
        LOG.info("applyToPost");
        try{
            RSUser user = em.find(RSUser.class, userId);
            RSPost post = em.find(RSPost.class, postId);
            
            LOG.log(Level.INFO, "{0} {1}", new Object[]{user.getFirstName(), user.getLastName()});
            
            if(!apply(post.getUsers(), userId)){
                LOG.info("applying...");
                
                post.getUsers().add(user);
                user.setPost(post);
            }
            return true;
        } catch(Exception e){
            LOG.info("ERROR in applyToPost");
        }
        return false;
    }
    
    @Override
    public boolean discardFromPost(Long userId, Long postId) {
        LOG.info("discardFromPost");
        try{
            RSUser user = em.find(RSUser.class, userId);
            RSPost post = em.find(RSPost.class, postId);
            if(apply(post.getUsers(), userId)){
                boolean remove = post.getUsers().remove(user);
                LOG.log(Level.INFO, "Remove user from list {0}", remove);
                user.setPost(post);
                user.setStatus(RSStatus.wait); 
                em.flush();
            } else {
            }
            return true;
        } catch(Exception e){
            LOG.info("ERROR in discardPost");
        }
        return false;
    }
    
    @Override
    public String getRole(Integer id) {
        try{
            String role = RSRole.getType(id).name();
            return role;
        }  catch(Exception e){
            LOG.info("ERROR in getRole");
        }
        return null;
    }
    
    @Override
    public String getStatus(Integer id) {
        try {
            String status = RSStatus.getType(id).name();
            return status;
        } catch (Exception e) {
            LOG.info("ERROR in getStatus");
        }
        return null;
    }
    
    @Override
    public Integer getStatusByName(String status){
        try {
            return RSStatus.valueOf(status).getId();
        } catch (Exception e) {
            LOG.info("ERROR in getStatusByName");
        }
        return null;
    }
    
    @Override
    public List<String> getAllStatuses(){
        List<String> allStuses = new ArrayList<>();
        for(RSStatus status : RSStatus.values()){
            allStuses.add(status.name());
        }
        return allStuses;
    }
    
    @Override
    public List<PostDetails> getAllPosts() {
        LOG.info("getAllPosts");
        List<PostDetails> postsDetails = new ArrayList<>();
        try{
             Query query = em.createQuery("SELECT p FROM RSPost p");
             List<RSPost> posts = query.getResultList();
             for(RSPost post : posts){
                 PostDetails postDetail = copyRSPostToPostDetails(post);
                 postsDetails.add(postDetail);
             }
        } catch(Exception e){
            LOG.log(Level.SEVERE, "ERROR in getAllPosts: {0}", e);
        }
        return postsDetails;
    }
    
    @Override
    public List<UserPost> getAllUsersPosts(String postName, String statusName){
        List<UserPost> allUsersPosts = new ArrayList<>();
        
        System.out.println("postName: " + postName);
        
        if("all".equals(postName)){
            List<PostDetails> allPosts = this.getAllPosts();
            for(PostDetails post : allPosts){
                List<UserDetails> allUsers = this.getUsersApplingToPost(post.getName(), statusName);
                for(UserDetails user : allUsers){
                    UserPost userPost = new UserPost();
                    userPost.setFirstName(user.getFirstName());
                    userPost.setLastName(user.getLastName());
                    userPost.setEmail(user.getEmail());
                    userPost.setLink(user.getLink());
                    userPost.setStatus(this.getStatus(user.getStatusId()));

                    userPost.setName(post.getName());
                    userPost.setRequirements(post.getRequirements());

                    allUsersPosts.add(userPost);
                }
            }
        } else {
            PostDetails specificPost = this.getPost(postName);
            List<UserDetails> specificPosts = this.getUsersApplingToPost(specificPost.getName(), statusName);
            for(UserDetails user : specificPosts){
                    UserPost userPost = new UserPost();
                    userPost.setFirstName(user.getFirstName());
                    userPost.setLastName(user.getLastName());
                    userPost.setEmail(user.getEmail());
                    userPost.setLink(user.getLink());
                    userPost.setStatus(this.getStatus(user.getStatusId()));

                    userPost.setName(specificPost.getName());
                    userPost.setRequirements(specificPost.getRequirements());

                    allUsersPosts.add(userPost);
                }
        }

        return allUsersPosts;
    }
    
}
