/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RecruitmentSystem.Entity;

/**
 *
 * @author UserPc
 */
public enum RSRole {
    CANDIDATE(1), GENERAL_DIRECTOR(2);
    
    private final int id;
    
    private RSRole(int id){
        this.id = id;
    }
    
    public int getId(){
        return id;
    }
    
    public static RSRole getType(Integer id){
        if(id == null){
            return null;
        }
        for(RSRole role : RSRole.values()){
            if(id.equals(role.getId())){
                return role;
            }
        }
        throw new IllegalArgumentException("No matching type for id " + id);
    }
    
    public static RSRole getType(String role){
        switch(role){
            case "CANDIDATE":
                return CANDIDATE;
            case "GENERAL_DIRECTOR":
                return GENERAL_DIRECTOR;
            default:
                return null;
        }
    }
    
    @Override
    public String toString() {
	switch(this) {
            case CANDIDATE:
		return "CANDIDATE";
                    case GENERAL_DIRECTOR:
			return "GENERAL_DIRECTOR";
		default:
                    return null;
	}
    } 
}
