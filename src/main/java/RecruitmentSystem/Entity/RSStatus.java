/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RecruitmentSystem.Entity;

/**
 *
 * @author Samuel
 */
public enum RSStatus {
    undefined(1), wait(2), accepted(3), denied(4);
    
    private final int id;
    
    private RSStatus(int id) {
        this.id = id;
    }
    
    public int getId() {
        return id;
    }
    
    public static RSStatus getType(Integer id){
        if(id == null) {
            return null;
        }
        for(RSStatus status : RSStatus.values()) {
            if(id.equals(status.getId())) {
                return status;
            }
        }
        throw new IllegalArgumentException("No matching type for id " + id);
    }
    
    public static RSStatus getType(String status) {
        switch(status){
            case "undefined":
                return undefined;
            case "wait":
                return wait;
            case "accepted":
                return accepted;
            case "denied":
                return denied;
            default:
                return null;
        }
    }
    
    @Override
    public String toString(){
        switch(this){
            case undefined:
                return "undefined";
            case wait:
                return "wait";
            case accepted:
                return "accepted";
            case denied:
                return "denied";
            default:
                return null;
        }
    }
}
