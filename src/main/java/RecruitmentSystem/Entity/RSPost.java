/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RecruitmentSystem.Entity;

import java.util.Collection;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author UserPc
 */
@Entity
@Table(name = "RSPost")
public class RSPost implements java.io.Serializable{
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    
    @Column(unique = true)
    private String name;
    
    @Column(unique = true)
    private String requirements;
    
    @OneToMany(mappedBy = "post", fetch = FetchType.EAGER)
    private Collection<RSUser> users;
    
    public RSPost(){
    }
    
    public RSPost(String name, String requirements){
        this.name = name;
        this.requirements = requirements;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getRequirements() {
        return requirements;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setRequirements(String requirements) {
        this.requirements = requirements;
    }

    public Collection<RSUser> getUsers() {
        return users;
    }

    public void setUsers(Collection<RSUser> users) {
        this.users = users;
    }
    
    public void addUser(RSUser user){
        this.getUsers().add(user);
    }
    
    public void dropUser(RSUser user){
        this.getUsers().remove(user);
    }
}
