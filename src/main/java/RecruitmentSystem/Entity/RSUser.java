/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RecruitmentSystem.Entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author UserPc
 */
@Entity
@Table(name = "RSUser")
public class RSUser implements java.io.Serializable{    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    Long id;
    
    @Column(unique = true)
    private String email;
    
    private String password;
    
    private String link;
    
    private String firstName;
    
    private String lastName;
    
    private Integer roleId;
    
    private Integer statusId;
    
    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    RSPost post;
    
    public RSUser(){
    }
    
    public RSUser(String email, String password, String link, String firstName, 
            String lastName, int id, int statusId){
        this.email = email;
        this.password = password;
        this.link = link;
        this.firstName = firstName;
        this.lastName = lastName; 
        this.roleId = id;
        this.statusId = statusId;
    }

    public Long getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public String getLink() {
        return link;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }
    
    public RSRole getRole(){
        return RSRole.getType(this.roleId);
    }

    public RSPost getPost() {
        return post;
    }
    
    public RSStatus getStatus() {
        return RSStatus.getType(this.statusId);
    }
    
    public void setId(Long id) {
        this.id = id;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setPost(RSPost post) {
        this.post = post;
    }
    
    public void setRole(RSRole role){
        if(role == null){
            this.roleId = null;
        } else {
            this.roleId = role.getId();
        }
    }
    
    public void setStatus(RSStatus status) {
        if(status == null){
            this.statusId = null;
        } else {
            this.statusId = status.getId();
        }
    }
}
