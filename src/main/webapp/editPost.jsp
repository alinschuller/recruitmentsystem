<%-- 
    Document   : editPost
    Created on : Jan 9, 2016, 9:53:04 PM
    Author     : Samuel
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Edit Post</title>
        <link href="css/style.css" rel='stylesheet' type='text/css' />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" type="image/x-icon" href="images/fav-icon.png" />
        <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
        <script type="text/javascript" src="js/jquery.min.js"></script>
        <script src="js/login.js"></script>
        <link type="text/css" rel="stylesheet" href="css/jquery.mmenu.all.css" />
        <script type="text/javascript" src="js/jquery.mmenu.js"></script>
        <script type="text/javascript">
            $(function () {
                var lastTd;
                var oldValue;
                var name = document.getElementById("name");
                var req = document.getElementById("req");
                var row;
                $("#submit-edit-post").hide();
                $("#cancel-edit-post").hide();
                $("#name").hide();
                $("#req").hide();
                $('nav#menu-left').mmenu();
                
                $("table tr.rows").on('click', function() {
                    if(lastTd === undefined) {
                        lastTd = this.cells[2];
                        oldValue = lastTd.innerHTML;
                    } else {
                        //lastTd.setAttribute('contenteditable', 'false');
                        lastTd = this.cells[2];
                        oldValue = lastTd.innerHTML;
                    }
                    var nameTd = this.cells[1];
                    name.value = nameTd.innerHTML;
                    var reqTd = this.cells[2];
                    req.value = reqTd.innerHTML;
                    this.cells[2].setAttribute('contenteditable', 'true');
                    $("#submit-edit-post").show();
                    $("#cancel-edit-post").show();
                });
                
                $("#cancel-edit-post").on('click', function() {
                    $("#submit-edit-post").hide();
                    $("#cancel-edit-post").hide();
                    lastTd.setAttribute('contenteditable', 'false');
                    lastTd.innerHTML = oldValue;
                });
                
                $(".reqTd").on('keyup', function() {
                    req.value = lastTd.innerHTML;;
                });
            });
            
            
        </script>
    </head>
    <body>
        <jsp:include page="headerDirector.jsp"/>
        <div class="tablearraddpost">
            <form action="EditPost" method="post">
                <table id="myTable">
                    <tr>
                        <th scope="col">Id post</th>
                        <th scope="col">Post name</th>
                        <th scope="col">Requirements</th>
                    </tr>
                    <c:forEach var="post" items="${requestScope.allPosts}">
                        
                        <tr class="rows">
                            <td scope="col"> <c:out value="${post.getId()}"/> </td>
                            <td scope="col" class="nameTd"> <c:out value="${post.getName()}"/> </td>
                            <td scope="col" class="reqTd"> <c:out value="${post.getRequirements()}"/> </td>           
                        <tr>
                            
                    </c:forEach>  
                </table>    
                </div>
        
                <input type="hidden" name="name" id="name" value=""/>
                <input type="hidden" name="req" id="req" value=""/>
                <div class="centering">
                <input class="editsubmitbutton" type="submit" name="submit" value="submit" id="submit-edit-post"/>
                <input class="editcancelbutton" type="reset" name="cancel" value="cancel" id="cancel-edit-post"/></div>
            </form>
        
    </body>
</html>
