<%-- 
    Document   : applyForPost
    Created on : Jan 8, 2016, 12:49:52 AM
    Author     : UserPc
--%>
<% Integer role_id = (Integer) session.getAttribute("userRoleId");
        Integer candidate = 1;
        if(!candidate.equals(role_id)){
                response.sendRedirect("");	
        }
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <title>Apply for post</title>
        <link href="css/style.css" rel='stylesheet' type='text/css' />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" type="image/x-icon" href="images/fav-icon.png" />
        <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
        <script type="text/javascript" src="js/jquery.min.js"></script>
        <script src="js/login.js"></script>
        <link type="text/css" rel="stylesheet" href="css/jquery.mmenu.all.css" />
        <script type="text/javascript" src="js/jquery.mmenu.js"></script>
        <script type="text/javascript">
            $(function () {
                $('nav#menu-left').mmenu();
            });
        </script>
    </head>
    <body>
        <jsp:include page="headerCandidate.jsp"/>
        
        <div>
            
            <form action="ApplyToPost" method="post">
                <br>
           <div class="tablearr">
            <table>
                    <tr>
                        <th scope="col">Post Name</th>
                        <th scope="col">Post Requirements</th>
                        <th scope="col">Action</th>
                    </tr>

                    <c:forEach var="post" items="${requestScope.allPosts}">
                        <tr>
                            <td> <c:out value="${post.getName()}"/> </td>
                            <td> <c:out value="${post.getRequirements()}"/> </td>   
                            <td> <a href="CandidatePost?postName=${post.getName()}" style="color: greenyellow"/> Apply </td>
                        </tr>
                    </c:forEach>
            </table>
               </div>
            </form>
        </div>
    </body>
</html>
