<%-- 
    Document   : uploadcv
    Created on : Dec 21, 2015, 1:22:17 PM
    Author     : alin.schueller
--%>

<% Integer role_id = (Integer) session.getAttribute("userRoleId");
        Integer candidate = 1;
        if(!candidate.equals(role_id)){
                response.sendRedirect("");	
        }
%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Upload CV</title>
        <link href="css/style.css" rel='stylesheet' type='text/css' />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" type="image/x-icon" href="images/fav-icon.png" />
        <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
        <script type="text/javascript" src="js/jquery.min.js"></script>
        <script src="js/login.js"></script>
        <link type="text/css" rel="stylesheet" href="css/jquery.mmenu.all.css" />
        <script type="text/javascript" src="js/jquery.mmenu.js"></script>
        <script type="text/javascript">
            $(function () {
                $('nav#menu-left').mmenu();
            });
        </script>
    </head>
    <body>
        <jsp:include page="headerCandidate.jsp"/>
        
        <div class="tablearrcv">
            <img class="driveimg" src="images/drive.png" alt="Google Drive" style="width:304px;height:180px;"><br/><br>
        <form action="UploadCv" method="post">
            <label>Put your google drive link: </label>
            <input type="text" name="cvLink" style="width: 500px"><br/>
            <input id="uploadcvbutton"type="submit" value="Upload">
        </form>
        </div>
    </body>
</html>
