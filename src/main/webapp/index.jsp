<%-- 
    Document   : index
    Created on : Dec 21, 2015, 1:22:00 PM
    Author     : alin.schueller
--%>
<%
    Integer role_id = (Integer) session.getAttribute("userRoleId");
    Integer candidate = 1;
    Integer director = 2;
    if (candidate.equals(role_id)) {
        response.sendRedirect("UploadCv");
    }
    if (director.equals(role_id)) {
        response.sendRedirect("ViewList");
    }
%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Online Recruitment System</title>
        <link href="css/style.css" rel='stylesheet' type='text/css' />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" type="image/x-icon" href="images/fav-icon.png" />
        <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
        <script type="text/javascript" src="js/jquery.min.js"></script>
        <link rel="stylesheet" type="text/css" href="css/slider-style.css" />
        <script type="text/javascript" src="js/modernizr.custom.28468.js"></script>
        <script src="js/login.js"></script>
        <script src="js/signup.js"></script>
        <link type="text/css" rel="stylesheet" href="css/jquery.mmenu.all.css" />
        <script type="text/javascript" src="js/jquery.mmenu.js"></script>
        <script type="text/javascript">
            $(function () {
                $('nav#menu-left').mmenu();
            });
            function valid()
            {
                var email = document.forms["loginForm"]["email"].value;
                var pass = document.forms["loginForm"]["password"].value;

                if ((email === null || email === "") || (pass === null || pass === ""))
                {
                    alert("All fields need to be completed!");
                    return false;
                }
                return true;
            }

            function validUp() {

                var email = document.forms["signupForm"]["email"].value;
                var pass = document.forms["signupForm"]["password"].value;
                var firstName = document.forms["signupForm"]["firstName"].value;
                var lastName = document.forms["signupForm"]["lastName"].value;

                if ((email === null || email === "") || (pass === null || pass === "") || (firstName === null || firstName === "") || (lastName === null || lastName === ""))
                {
                    alert("All fields need to be completed!");
                    return false;
                }
                if (pass.length < 5)
                {
                    alert("Your password need to be 5 characters or greater.");
                    return false;
                }
                if (!validateEmail(email)) {
                    alert("Your email should be example@something.anything");
                    return false;
                }
                return true;
            }

            function validateEmail(email) {
                var re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                return re.test(email);
            }
        </script>
    </head>
    <body>
        <div class="header">
            <div class="wrap">
                <div class="header-left">
                    <div class="logo">
                        <a href="index.jsp">Recruitment System</a>
                    </div>
                </div>
                <div class="header-right">
                    <div class="sign-ligin-btns">
                        <ul>
                            <li id="loginContainer"><a class="login" id="loginButton" href="#"><span><i>Login</i></span></i></a>
                                <div class="clear"> </div>
                                <div id="loginBox">                
                                    <form id="loginForm" action="LoginServlet" method="POST" onsubmit="return valid()">
                                        <fieldset id="body">
                                            <fieldset>
                                                <label for="email">Email Address</label>
                                                <input type="text" name="email" id="email" />
                                            </fieldset>
                                            <fieldset>
                                                <label for="password">Password</label>
                                                <input type="password" name="password" id="password" />
                                            </fieldset>
                                            <label class="remeber" for="checkbox"><input type="checkbox" id="checkbox" />Remember me</label>
                                            <input type="submit" id="login" value="login" />
                                        </fieldset>
                                    </form>
                                </div>
                            </li>
                            <li id="signupContainer"><a class="signup" id="signupButton" href="#"><span><i>Sign Up</i></span></i></a>
                                <div class="clear"> </div>
                                <div id="signupBox">                
                                    <form id="signupForm" action="SignUp" method="POST" onsubmit="return validUp()">
                                        <fieldset id="body">
                                            <fieldset>
                                                <label for="email">Email Address</label>
                                                <input type="text" name="email" id="email" />
                                            </fieldset>
                                            <fieldset>
                                                <label for="password">Password</label>
                                                <input type="password" name="password" id="password" />
                                            </fieldset>
                                            <fieldset>
                                                <label for="firstName">First Name</label>
                                                <input type="text" name="firstName" id="firstName" />
                                            </fieldset>
                                            <fieldset>
                                                <label for="lastName">Last Name</label>
                                                <input type="text" name="lastName" id="lastName" />
                                            </fieldset>
                                            <input type="submit" id="signup" value="signup" />
                                        </fieldset>
                                    </form>
                                </div>
                            </li>
                            <div class="clear"> </div>
                        </ul>
                    </div>
                    <div class="clear"> </div>
                </div>
                <div class="clear"> </div>
            </div>
            <p><%= request.getParameter("error") == null ? "" : request.getParameter("error")%></p>
        </div> 
        <div class="text-slider">
            <div class="wrap"> 
                <div id="da-slider" class="da-slider">
                    <div class="da-slide">
                        <h2>Welcome to our Recruitment System</h2>
                    </div>
                    <div class="da-slide">
                        <h2>Sign up</h2>
                    </div>
                    <div class="da-slide">
                        <h2>Upload your Google Drive link</h2>
                    </div>
                    <div class="da-slide">
                        <h2>Apply for a post</h2>
                    </div>
                    <div class="da-slide">
                        <h2>Relax and wait for results</h2>
                    </div>
                </div>

                <nav class="da-arrows">
                    <span class="da-arrows-prev"> </span>
                    <span class="da-arrows-next"> </span>
                </nav>
            </div>
            <script type="text/javascript" src="js/jquery.cslider.js"></script>
            <script type="text/javascript">
                                        $(function () {
                                            $('#da-slider').cslider({
                                                autoplay: true,
                                                bgincrement: 450
                                            });

                                        });
            </script>
        </div>
    </div>
</body>
</html>
