<%-- 
    Document   : viewList
    Created on : Jan 8, 2016, 2:04:02 PM
    Author     : UserPc
--%>
<% Integer role_id = (Integer) session.getAttribute("userRoleId");
        Integer director = 2;
        if(!director.equals(role_id)){
                response.sendRedirect("");	
        }
%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>View Candidates</title>
        <link href="css/style.css" rel='stylesheet' type='text/css' />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" type="image/x-icon" href="images/fav-icon.png" />
        <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
        <script type="text/javascript" src="js/jquery.min.js"></script>
        <script src="js/login.js"></script>
        <link type="text/css" rel="stylesheet" href="css/jquery.mmenu.all.css" />
        <script type="text/javascript" src="js/jquery.mmenu.js"></script>
        <script type="text/javascript">
            $(function () {
                $('nav#menu-left').mmenu();
            });
        </script>
    </head>
    <body>
        <jsp:include page="headerDirector.jsp"/>
        
        <p style="color: #0033CC">The list with the candidates which applied:</p>
        <br>
        <p style="color: #FF0000"> ${requestScope.msg} </p>
        <div>
            <div class="tablearr"><br>
                <form action="ViewList" method="post">
                    <c:out value="Choose a post: " />
                    <select name="selectedPost">
                        <option> all </option>
                        <c:forEach var="post" items="${requestScope.allPosts}">
                            <option> <c:out value="${post.getName()}" /> </option>
                        </c:forEach>
                    </select>
                    <c:out value="Choose a status: " />
                    <select name="selectedStatus">
                        <option> <c:out value="all" /> </option>
                        <c:forEach var="status" items="${requestScope.allStatuses}">
                            <option> <c:out value="${status}" /> </option>
                        </c:forEach>
                    </select>
                    <input type="submit" class="viewlistsubmit" value="View candidates sorted by criteria" />
                </form><br>
            </div><br>
            <div class="tablearr">
                <table>
                    <tr>
                        <th scope="col">Candidate First Name</th>
                        <th scope="col">Candidate Last Name</th>
                        <th scope="col">Candidate Email</th>
                        <th scope="col">Post Name</th>
                        <th scope="col">Post Requirements</th>
                        <th scope="col">Candidate CV</th>
                        <th scope="col">Actions</th>
                        <th scope="col">Post status</th>
                    </tr>
                    <c:forEach var="userPost" items="${requestScope.allUsersPosts}">
                        <tr>
                            <td> <c:out value="${userPost.getFirstName()}"/> </td>
                            <td> <c:out value="${userPost.getLastName()}"/> </td>
                            <td> <c:out value="${userPost.getEmail()}"/> </td>
                            <td> <c:out value="${userPost.getName()}"/> </td>
                            <td> <c:out value="${userPost.getRequirements()}"/> </td>
                            <td> <a href="${userPost.getLink()}"> ${userPost.getFirstName()}'s CV </a> </td>
                            <td> <a style="color: #00FF00" href="ViewList?processed=true&email=${userPost.getEmail()}&status=accepted"> Accept </a> 
                                 <a style="color: #FF0000" href="ViewList?processed=true&email=${userPost.getEmail()}&status=denied"> Deny </a> </td>
                            <td> <c:out value="${userPost.getStatus()}"/> </td>
                        </tr>
                    </c:forEach>
                </table>
            </div>
        </div>
    </body>
</html>
