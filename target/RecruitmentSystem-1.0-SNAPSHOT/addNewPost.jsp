<%-- 
    Document   : addNewPost
    Created on : Jan 9, 2016, 9:52:40 PM
    Author     : Samuel
--%>
<% Integer role_id = (Integer) session.getAttribute("userRoleId");
    Integer director = 2;
    if (!director.equals(role_id)) {
        response.sendRedirect("");
    }
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Add new post</title>
        <link href="css/style.css" rel='stylesheet' type='text/css' />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" type="image/x-icon" href="images/fav-icon.png" />
        <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
        <script type="text/javascript" src="js/jquery.min.js"></script>
        <script src="js/login.js"></script>
        <link type="text/css" rel="stylesheet" href="css/jquery.mmenu.all.css" />
        <script type="text/javascript" src="js/jquery.mmenu.js"></script>
        <script type="text/javascript">
            $(function () {
                $('nav#menu-left').mmenu();
            });
        </script>
    </head>
    <body>
        <jsp:include page="headerDirector.jsp"/>
        <div class="tablearraddpost">
            <form action="AddNewPost" method="post">
                <label> Post name: </label>
                <input type="text" name="postName" id="postName" style="width: 28%"/><br/><br/>
                <label> Requirements: </label>
                <textarea name="req" id="req" rows="4" cols="30"> </textarea><br/>
                </div>
                <input type="submit" id="addpostbutton" name="submit" value="submit"/>
            </form>
    </body>
</html>
