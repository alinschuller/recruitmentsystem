<%-- 
    Document   : candidatePost
    Created on : Jan 8, 2016, 1:26:21 PM
    Author     : UserPc
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<% Integer role_id = (Integer) session.getAttribute("userRoleId");
    Integer candidate = 1;
    if (!candidate.equals(role_id)) {
        response.sendRedirect("");
    }
%>
<!DOCTYPE html>
<html>
    <head>
        <title>Candidate Recruitment System</title>
        <link href="css/style.css" rel='stylesheet' type='text/css' />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" type="image/x-icon" href="images/fav-icon.png" />
        <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
        <script type="text/javascript" src="js/jquery.min.js"></script>
        <script src="js/login.js"></script>
        <link type="text/css" rel="stylesheet" href="css/jquery.mmenu.all.css" />
        <script type="text/javascript" src="js/jquery.mmenu.js"></script>
        <script type="text/javascript">
            $(function () {
                $('nav#menu-left').mmenu();
            });
        </script>
    </head>
    <body>
        <jsp:include page="headerCandidate.jsp"/>

        <p style="color: #0033CC"> ${requestScope.message} </p> <br>
        <div class="tablearr">
        <form action="CandidatePost" method="post">
            
                <table>
                    <tr>
                        <th scope="col">Post Name</th>
                        <th scope="col">Post Requirements</th>  
                        <th scope="col">Post Status</th>   
                    </tr>
                    <tr>
                        <td> <c:out value="${requestScope.postName}"/> </td>
                        <td> <c:out value="${requestScope.postReq}"/> </td>
                        <td> <c:out value="${requestScope.postStatus}"/> </td>
                    </tr>                
                </table>
            
        </form>
</div>
    </body>
</html>

