<%-- 
    Document   : changePassword
    Created on : Jan 10, 2016, 4:34:52 PM
    Author     : UserPc + alin.schuller
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Change Password</title>
        <link href="css/style.css" rel='stylesheet' type='text/css' />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" type="image/x-icon" href="images/fav-icon.png" />
        <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
        <script type="text/javascript" src="js/jquery.min.js"></script>
        <script src="js/login.js"></script>
        <link type="text/css" rel="stylesheet" href="css/jquery.mmenu.all.css" />
        <script type="text/javascript" src="js/jquery.mmenu.js"></script>
        <script type="text/javascript">
            $(function () {
                /*$("#msg").hide();
                if($("#msg").text() !== ""){
                    alert($("#paragraf").text());
                }*/
                $('nav#menu-left').mmenu();
            });
        </script>
    </head>
    <body>
        <jsp:include page="${requestScope.menu}.jsp"/>
        <div class="tablearrchangepsw">
        <p id="msg" style="color: #FF0000"><%= request.getParameter("changePassMsg") == null ? "" :  request.getParameter("changePassMsg")%></p><br/>
        <form action="ChangePassword" method="post">
            <label> Old password: </label>
            <input type="password" name="oldPassword" style="width: 100px"><br/>
            <label> New password: </label>
            <input type="password" name="newPassword" style="width: 100px"><br/>
            <label> Confirm password: </label>
            <input type="password" name="confirmPassword" style="width: 100px"><br/><br>
            </div>
            <input id="changepswbutton" type="submit" value="Change password">
        </form>
        
    </body>
</html>