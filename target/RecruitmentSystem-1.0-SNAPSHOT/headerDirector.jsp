<%-- 
    Document   : headerDirector
    Created on : Jan 8, 2016, 2:01:28 PM
    Author     : UserPc
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <div class="header">
            <div class="wrap">
                <div class="header-left">
                    <div class="logo">
                        <a href="index.jsp">Recruitment System</a>
                    </div>
                </div>
                <div class="header-right">
                    <div class="top-nav">
                        <ul>
                            <li><a href="ViewList">View list</a></li>
                            <li><a href="EditPost">Edit post</a></li>
                            <li><a href="addNewPost.jsp">Add post</a></li>
                            <li><a href="ChangePassword">Change Password</a></li>
                            <li><a href="LogOut">Log out</a></li>
                        </ul>
                    </div>

                    <div class="clear"> </div>
                </div>
                <div class="clear"> </div>
            </div>
        </div>
    </body>
</html>
